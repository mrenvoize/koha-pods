
  The [KohaPOD]() website.

## Working Locally

  Clone the repository.

    $ git clone git://github.com/mrenvoize/koha-pod.git
    $ cd koha-pod

  Install [Mojolicious](http://mojolicious.org).

    $ curl -L https://cpanmin.us | perl - -M https://cpan.metacpan.org -n Mojolicious

  Start hacking.

    $ morbo kohapod.pl

## Copyright And License

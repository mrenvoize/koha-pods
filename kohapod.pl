#!/usr/bin/env perl

use Mojolicious::Lite;
use FindBin;
BEGIN { unshift @INC, "$FindBin::Bin/lib" }

plugin 'KohaPOD';

# Documentation
any '/:module' => {module => 'Koha/Contributing', format => undef} => [module => qr/[^.]+/, format => ['txt']] =>
  app->perldoc;

app->start;
